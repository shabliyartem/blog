// Default router configurations
Router.configure({
  // A layout template to be used
  layoutTemplate: 'applicationLayout',
  // Which template to render in named yield regions
  yieldRegions: {
    'sideMenu': {to: 'sideBlock'}
  },
  // Default data context for the template
  data: function() {
    return {pageTitle: 'Welcome to Blog', pageDescription: 'Blog powered by meteor' };
  }
});

Router.route('/', {
  name: 'home',
  template: 'postsIndex'
});

Router.route('/posts', {
  name: 'postsIndex',
  yieldRegions: {
    'postsMenu': {to: 'sideBlock'}
  },
  data: function() {
    return {pageTitle: 'All Posts', pageDescription: 'List of all posts in our Blog'};
  }
});

Router.route('/posts/new', {
  name: 'postNew'
});

Router.route('/posts/:id', {
  name: 'postShow',
  data: function() {
    return Posts.findOne({_id: this.params.id});
  }
});

Router.route('/posts/:id/edit', {
  name: 'postEdit',
  data: function() {
    return Posts.findOne({_id: this.params.id});
  }
});
