function permitted(postData) {
  return _.pick(postData, 'title', 'content');
}

Meteor.methods({
  postCreate: function (post) {
    var id = Posts.insert(
      _.extend(permitted(post), {createdAt: new Date(), updatedAt: new Date()})
    );
    if (Meteor.isClient) {
      Router.go('postShow', {id: id});
    }
  },

  postUpdate: function (id, post) {
    Posts.update(id,
      {$set: _.extend(permitted(post), {createdAt: new Date(), updatedAt: new Date()}) }
    );
    if (Meteor.isClient) {
      Router.go('postShow', {id: id});
    }
  },

  postDestroy: function(id) {
    Posts.remove(id);
  }
});
