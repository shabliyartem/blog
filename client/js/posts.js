Meteor.subscribe("posts");

Template.postsIndex.helpers({
  posts: function() {
    return Posts.find({}, {sort: {createdAt: -1}});
  }
});

Template.postsIndex.events({
  'click .post-destroy': function() {
    Meteor.call("postDestroy", this._id);
    return false;
  }
});

Template.postShow.events({
  'click .post-destroy': function() {
    Meteor.call("postDestroy", this._id);
    Router.go('postsIndex');
    return false;
  }
});

Template.postNew.events({
  'submit form#post-form': function(evt) {
    Meteor.call("postCreate", ViewHelpers.getFormData(evt.target));
    return false;
  }
});

Template.postEdit.events({
  'submit form#post-form': function(evt) {
    Meteor.call("postUpdate", this._id, ViewHelpers.getFormData(evt.target));
    return false;
  }
});
