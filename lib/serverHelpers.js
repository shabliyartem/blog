if (Meteor.isServer) {
  var asyncTimeout = function(delay, callback) {
    setTimeout(function() { callback(); }, delay);
  };

  ServerHelpers = {
    sleep: Meteor.wrapAsync(asyncTimeout)
  };
}
