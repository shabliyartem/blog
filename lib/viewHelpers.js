if (Meteor.isClient) {
  ViewHelpers = {
    getFormData: function(form) {
      return _.reduce($(form).serializeArray(), function(memo, input) {
        memo[input.name] = input.value;
        return memo;
      }, {});
    }
  };
}

UI.registerHelper('pathTo', function(routeName, id) {
  return Router.path(routeName, {id: id});
});
