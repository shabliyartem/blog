#!/bin/bash
# Meteor seeds script

# Enter seeds directory
cd "$( dirname "${BASH_SOURCE[0]}" )"

echo "You must already have your application running locally with 'meteor run' command"

while true; do
  read -p "Is meteor running now? yes/no: " yn
  case $yn in
    [Yy]* ) echo "Starting seeds"; break;;
    [Nn]* ) echo "Exiting"; exit;;
    * ) echo "Please answer yes or no: ";;
  esac
done

mongo meteor --host 127.0.0.1 --port 3001 seeds.mongo.js

while true; do
  read -p "Press any key to exit..." -n 1
  echo
  exit
done
